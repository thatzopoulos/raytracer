# Raytracer C++

![Raytracer
Output](output-examples/100-samples-per-pixel-1920x1080.png?raw=true "example")
C++ implementation of the Ray Tracer described in the book Ray Tracing In One
Weekend.

My Rust port of this project can be found [here.](https://gitlab.com/thatzopoulos/starforge)

# Main Features
While relatively simple so far, the raytracer implements some advanced features
including:
- reflection
- refraction
- indirect lighting
- Multiple materials: Lambertian, Dialectric, Metal
- Blur

# Setup

## Build and Run
`build.sh` compiles the program using clang

`run.sh` recompiles the code, pipes the output to a ppm file and then opens it with chromium.

# Examples

Example images from various stages of implementation can be found in
`output-examples`


